<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_author', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('photo')->nullable(true);
            $table->string('name');
            $table->string('position')->nullable(true);
            $table->text('description');

            $table->string('url_key');
            $table->string('meta_title')->nullable(true);
            $table->text('meta_description')->nullable(true);
            $table->string('og_title')->nullable(true);
            $table->text('og_description')->nullable(true);
            $table->string('og_image')->nullable(true);
            
            $table->timestamps();
        });

        Schema::create('blog_author_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');
            
            $table->timestamps();
            
            $table->foreign('author_id')
                ->references('id')
                ->on('blog_author')
                ->onDelete('cascade');
        });

        Schema::create('blog_post', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');

            $table->tinyInteger('status');
            $table->string('thumbnail')->nullable(true);
            $table->string('title');
            $table->text('content');

            $table->string('url_key');
            $table->string('meta_title')->nullable(true);
            $table->text('meta_description')->nullable(true);
            $table->string('og_title')->nullable(true);
            $table->text('og_description')->nullable(true);
            $table->string('og_image')->nullable(true);


            $table->timestamps();

            $table->foreign('author_id')
                ->references('id')
                ->on('blog_author')
                ->onDelete('cascade');
        });

        Schema::create('blog_post_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            
            $table->timestamps();
            
            $table->foreign('post_id')
                ->references('id')
                ->on('blog_post')
                ->onDelete('cascade');
        });

        Schema::create('blog_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable(true);

            $table->tinyInteger('status');
            $table->string('image')->nullable(true);
            $table->string('title');
            $table->text('content')->nullable(true);

            $table->string('url_key');
            $table->string('meta_title')->nullable(true);
            $table->text('meta_description')->nullable(true);
            $table->string('og_title')->nullable(true);
            $table->text('og_description')->nullable(true);
            $table->string('og_image')->nullable(true);
            
            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('blog_category')
                ->onDelete('set null');
        });

        Schema::create('blog_category_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            
            $table->timestamps();
            
            $table->foreign('category_id')
                ->references('id')
                ->on('blog_category')
                ->onDelete('cascade');
        });

        Schema::create('blog_tag', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->tinyInteger('status');
            $table->string('title');
            $table->text('content')->nullable(true);

            $table->string('url_key');
            $table->string('meta_title')->nullable(true);
            $table->text('meta_description')->nullable(true);
            $table->string('og_title')->nullable(true);
            $table->text('og_description')->nullable(true);
            $table->string('og_image')->nullable(true);
            
            $table->timestamps();
        });

        Schema::create('blog_tag_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tag_id');
            
            $table->timestamps();
            
            $table->foreign('tag_id')
                ->references('id')
                ->on('blog_tag')
                ->onDelete('cascade');
        });

        Schema::create('blog_post_category', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('category_id');
            
            $table->timestamps();

            $table->foreign('post_id')
                ->references('id')
                ->on('blog_post')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('blog_category')
                ->onDelete('cascade');
        });

        Schema::create('blog_post_tag', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('tag_id');
            
            $table->timestamps();

            $table->foreign('post_id')
                ->references('id')
                ->on('blog_post')
                ->onDelete('cascade');

            $table->foreign('tag_id')
                ->references('id')
                ->on('blog_tag')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_author');
        Schema::dropIfExists('blog_author_detail');
        Schema::dropIfExists('blog_post');
        Schema::dropIfExists('blog_post_detail');
        Schema::dropIfExists('blog_category');
        Schema::dropIfExists('blog_category_detail');
        Schema::dropIfExists('blog_tag');
        Schema::dropIfExists('blog_tag_detail');
        Schema::dropIfExists('blog_post_category');
        Schema::dropIfExists('blog_post_tag');
    }
}
