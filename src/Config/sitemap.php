<?php

$blogRoute = \Settings::getConfigValue('blog/route') ?: 'blog';

return [
    'generators' => [
        $blogRoute . '-posts'      => \WFN\Blog\Sitemap\Posts\Generator::class,
        $blogRoute . '-categories' => \WFN\Blog\Sitemap\Categories\Generator::class,
        $blogRoute . '-tags'       => \WFN\Blog\Sitemap\Tags\Generator::class,
        $blogRoute . '-authors'    => \WFN\Blog\Sitemap\Authors\Generator::class,
    ],
];