<?php
return [
    'search' => [
        'enabled' => env('SEARCH_ENABLED', false),
        'hosts' => explode(',', env('SEARCH_HOSTS')),
        'index_prefix' => env('SEARCH_INDEX_PREFIX', 'laravel_'),
    ],
];