<?php

return [
    'blog' => [
        'label'  => 'Blog',
        'fields' => [
            'title' => [
                'label' => 'Title',
                'type'  => 'text',
            ],
            'description' => [
                'label' => 'Description',
                'type'  => 'textarea',
            ],
            'route' => [
                'label' => 'Blog Route',
                'type'  => 'text',
            ],
        ],
    ],
];