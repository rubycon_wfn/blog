<?php
return [
    'blog' => [
        'label' => \Settings::getConfigValue('blog/root_navigation_label') ?: 'Blog',
        'icon'  => 'icon-speech',
        'childrens' => [
            'post' => [
                'route' => 'admin.blog.post',
                'label' => \Settings::getConfigValue('blog/posts_navigation_label') ?: 'Posts',
            ],
            'category' => [
                'route' => 'admin.blog.category',
                'label' => \Settings::getConfigValue('blog/categories_navigation_label') ?: 'Categories',
            ],
            'tag' => [
                'route' => 'admin.blog.tag',
                'label' => \Settings::getConfigValue('blog/tags_navigation_label') ?: 'Tags',
            ],
            'author' => [
                'route' => 'admin.blog.author',
                'label' => \Settings::getConfigValue('blog/authors_navigation_label') ?: 'Authors',
            ],
        ],
    ],
];