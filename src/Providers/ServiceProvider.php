<?php
namespace WFN\Blog\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use WFN\Blog\Model\Post\Repository as PostRepository;
use WFN\Blog\Model\Post\EloquentRepository as PostEloquentRepository;
use WFN\Blog\Model\Post\ElasticsearchRepository as PostElasticsearchRepository;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ServiceProvider extends WFNServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \WFN\Blog\Console\Commands\BlogIndexer::class
        ]);

        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('BlogPost', \WFN\Blog\Model\Post::class);
            $loader->alias('BlogCategory', \WFN\Blog\Model\Category::class);
            $loader->alias('BlogAuthor', \WFN\Blog\Model\Author::class);
            $loader->alias('BlogTag', \WFN\Blog\Model\Tag::class);
        });

        $this->app->singleton(PostRepository::class, function($app) {
            if(!config('services.search.enabled')) {
                return new PostEloquentRepository();
            }

            return new PostElasticsearchRepository(
                $app->make(Client::class)
            );
        });

        $this->bindSearchClient();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        
        Route::middleware('web')->group($basePath . '/routes/web.php');
        Route::middleware('api')->group($basePath . '/routes/api.php');
        
        $this->loadViewsFrom($basePath . '/views', 'blog');
        $this->publishes([
            $basePath . '/views' => resource_path('views/blog'),
        ], 'view');

        $this->loadMigrationsFrom($basePath . '/database/migrations');
        
        $this->mergeConfigFrom($basePath . '/Config/sitemap.php', 'sitemap');
        $this->mergeConfigFrom($basePath . '/Config/widgets.php', 'widgets');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
        $this->mergeConfigFrom($basePath . '/Config/settings.php', 'settings');
        $this->mergeConfigFrom($basePath . '/Config/services.php', 'services');

        \BlogPost::bootSearchable();
    }

    private function bindSearchClient()
    {
        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts(config('services.search.hosts'))
                ->build();
        });
    }

}
