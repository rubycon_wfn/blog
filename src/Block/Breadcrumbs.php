<?php

namespace WFN\Blog\Block;

use Illuminate\Support\Facades\View;

class Breadcrumbs
{

    protected $_crumbs = [];

    public static function getInstance()
    {
        return new self();
    }

    public function render()
    {
        $viewName = View::exists('blog.html.breadcrumbs') ? 'blog.html.breadcrumbs' : 'blog::html.breadcrumbs';
        $crumbs = $this->getCrumbs();
        $breadcrumbs = $this;
        return view($viewName, compact('crumbs', 'breadcrumbs'));
    }

    public function getCrumbs()
    {
        if(empty($this->_crumbs)) {
            if(!empty($this->post)) {
                $this->_crumbs[] = [
                    'url'   => false,
                    'title' => $this->post->title,
                ];
            }

            if(!empty($this->category)) {
                $currentCategory = $this->category;
                $this->_crumbs[] = [
                    'url'   => !empty($this->post) ? url($currentCategory->getUrlPath()) : false,
                    'title' => $currentCategory->title,
                ];

                while($currentCategory->parent) {
                    $this->_crumbs[] = [
                        'url'   => url($currentCategory->parent->getUrlPath()),
                        'title' => $currentCategory->parent->title,
                    ];
                    $currentCategory = $currentCategory->parent;
                }
            }

            $blogRoute = (\Settings::getConfigValue('blog/route') ?: 'blog') . '/';

            if(!empty($this->author)) {
                $this->_crumbs[] = [
                    'url'   => !empty($this->post) ? url($blogRoute . $this->author->url_key) : false,
                    'title' => $this->author->name,
                ];
            }

            if(!empty($this->tag)) {
                $this->_crumbs[] = [
                    'url'   => !empty($this->post) ? url($blogRoute . $this->tag->url_key) : false,
                    'title' => $this->tag->title,
                ];
            }

            $this->_crumbs[] = [
                'url'   => count($this->_crumbs) ? url(\Settings::getConfigValue('blog/route') ?: 'blog') : false,
                'title' => \Settings::getConfigValue('blog/title') ?: 'Blog',
            ];

            $this->_crumbs[] = [
                'url'   => url('/'),
                'title' => 'Home',
            ];

            $this->_crumbs = array_reverse($this->_crumbs);
        }
        return $this->_crumbs;
    }

    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }


}