<?php

namespace WFN\Blog\Block\Admin\Post;

class Grid extends \WFN\Blog\Block\Admin\BlogEntity\Grid
{

    protected $filterableFields = ['title', 'author_id', 'url_key', 'status'];

    protected $adminRoute = 'admin.blog.post';

    protected $entityType = 'post';

    public function getInstance()
    {
        return new \BlogPost();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('title', 'Title');
        $this->addColumn('author_id', \Settings::getConfigValue('blog/authors_navigation_label') ?: 'Authors', 'select', true, new \WFN\Blog\Model\Source\Authors());
        $this->addColumn('url_key', 'URL Key');
        $this->addColumn('status', 'Status', 'select', true, new \WFN\CMS\Model\Source\Status());
        return parent::_beforeRender();
    }

    public function getTitle()
    {
        return 'Manage ' . (\Settings::getConfigValue('blog/posts_navigation_label') ?: 'Posts');
    }

}