<?php

namespace WFN\Blog\Block\Admin\Post;

class Form extends \WFN\Blog\Block\Admin\BlogEntity\Form
{

    protected $adminRoute = 'admin.blog.post';

    protected $entityType = 'post';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'thumbnail', 'Thumbnail', 'image', ['required' => false]);
        $this->addField('general', 'title', 'Title', 'text', ['required' => true]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        $this->addField('general', 'author_id', \Settings::getConfigValue('blog/authors_navigation_label') ?: 'Authors', 'select', [
            'required' => true,
            'source'   => \WFN\Blog\Model\Source\Authors::class,
        ]);

//        $this->addField('content', 'content', 'Content', 'wysiwyg', ['required' => true]);

        $this->addField('taxonomies', 'category_ids', \Settings::getConfigValue('blog/categories_navigation_label') ?: 'Categories', 'multiselect', [
            'required' => false,
            'source'   => \WFN\Blog\Model\Source\Categories::class,
        ]);
        $this->addField('taxonomies', 'tag_ids', \Settings::getConfigValue('blog/tags_navigation_label') ?: 'Tags', 'multiselect', [
            'required' => false,
            'source'   => \WFN\Blog\Model\Source\Tags::class,
        ]);

        return parent::_beforeRender();
    }

}