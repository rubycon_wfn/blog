<?php

namespace WFN\Blog\Block\Admin\BlogEntity;

abstract class Grid extends \WFN\Admin\Block\Widget\AbstractGrid
{


    protected function _beforeRender()
    {
        $this->_addAdditionalColumns();
        return parent::_beforeRender();
    }

    protected function _addAdditionalColumns()
    {
        if($this->entityType) {
            foreach(config($this->entityType . 'DetailFields', []) as $group => $fields) {
                foreach($fields as $field => $data) {
                    if(!empty($data['grid'])) {
                        $this->addColumn($field, $data['label'], $data['type'], false);
                    }
                }
            }
        }
    }
}