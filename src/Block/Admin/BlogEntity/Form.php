<?php

namespace WFN\Blog\Block\Admin\BlogEntity;

class Form extends \WFN\Admin\Block\Widget\AbstractForm
{

    protected $adminRoute = 'admin.blog.post';

    protected $entityType;

    protected function _beforeRender()
    {
        $this->_addSeoFields();
        $this->_addAdditionalFields();
        return parent::_beforeRender();
    }

    protected function _addSeoFields()
    {
        $this->addField('search_engine_optimization', 'url_key', 'URL Key', 'text', ['required' => true]);
        $this->addField('search_engine_optimization', 'meta_title', 'Meta Title', 'text', ['required' => false]);
        $this->addField('search_engine_optimization', 'meta_description', 'Meta Description', 'textarea', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_title', 'OG Title', 'text', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_description', 'OG Description', 'textarea', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_image', 'OG Image', 'image', ['required' => false]);
    }

    protected function _addAdditionalFields()
    {
        if($this->entityType) {
            foreach(config($this->entityType . 'DetailFields', []) as $group => $fields) {
                foreach($fields as $field => $data) {
                    $options = (!empty($data['options']) && is_array($data['options'])) ? $data['options'] : [];

                    if($data['type'] == 'image' || $data['type'] == 'file') {
                        $options['publicValue'] = $this->getInstance()->getAttributeUrl($field);
                    }

                    if($data['type'] == 'date' && $this->getInstance()->getAttribute($field)) {
                        $options['value'] = $this->getInstance()->getAttribute($field)->format('Y-m-d');
                    }

                    $this->addField($group, $field, $data['label'], $data['type'], $options);
                }
            }
        }
    }

}