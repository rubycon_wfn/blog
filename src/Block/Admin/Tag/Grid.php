<?php

namespace WFN\Blog\Block\Admin\Tag;

class Grid extends \WFN\Blog\Block\Admin\BlogEntity\Grid
{

    protected $filterableFields = ['title', 'url_key', 'status'];

    protected $adminRoute = 'admin.blog.tag';

    protected $entityType = 'tag';

    public function getInstance()
    {
        return new \BlogTag();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('title', 'Title');
        $this->addColumn('url_key', 'URL Key');
        $this->addColumn('status', 'Status', 'select', true, new \WFN\CMS\Model\Source\Status());
        return parent::_beforeRender();
    }

    public function getTitle()
    {
        return 'Manage ' . (\Settings::getConfigValue('blog/tags_navigation_label') ?: 'Tags');
    }

}