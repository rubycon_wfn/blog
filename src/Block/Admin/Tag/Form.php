<?php

namespace WFN\Blog\Block\Admin\Tag;

class Form extends \WFN\Blog\Block\Admin\BlogEntity\Form
{

    protected $adminRoute = 'admin.blog.tag';

    protected $entityType = 'tag';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'title', 'Title', 'text', ['required' => true]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        
        $this->addField('content', 'content', 'Content', 'wysiwyg', ['required' => true]);
        
        return parent::_beforeRender();
    }

}