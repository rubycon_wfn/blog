<?php

namespace WFN\Blog\Block\Admin\Category;

class Form extends \WFN\Blog\Block\Admin\BlogEntity\Form
{

    protected $adminRoute = 'admin.blog.category';

    protected $entityType = 'category';

    public function render()
    {
        $this->_beforeRender();
        return view('blog::admin.category.edit', [
            'form' => $this,
            'edit' => true,
        ]);
    }

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'image', 'Image', 'image', ['required' => false]);
        $this->addField('general', 'title', 'Title', 'text', ['required' => true]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        $this->addField('general', 'parent_id', 'Parent Category', 'select', [
            'required' => false,
            'source'   => \WFN\Blog\Model\Source\Categories::class,
        ]);

        $this->addField('content', 'content', 'Content', 'wysiwyg', ['required' => true]);
        
        return parent::_beforeRender();
    }

}