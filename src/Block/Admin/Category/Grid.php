<?php

namespace WFN\Blog\Block\Admin\Category;

class Grid extends \WFN\Blog\Block\Admin\BlogEntity\Grid
{

    protected $adminRoute = 'admin.blog.category';

    protected $entityType = 'category';

    public function getInstance()
    {
        return new \BlogCategory();
    }

    public function render()
    {
        $this->_beforeRender();
        return view('blog::admin.category.edit', [
            'form' => $this,
            'edit' => false,
        ]);
    }

    public function getTitle()
    {
        return 'Manage ' . (\Settings::getConfigValue('blog/categories_navigation_label') ?: 'Categories');
    }

}