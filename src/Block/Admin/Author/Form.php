<?php

namespace WFN\Blog\Block\Admin\Author;

class Form extends \WFN\Blog\Block\Admin\BlogEntity\Form
{

    protected $adminRoute = 'admin.blog.author';

    protected $entityType = 'author';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        $this->addField('general', 'name', 'Name', 'text', ['required' => true]);
        $this->addField('general', 'photo', 'Photo', 'image', ['required' => false]);
        $this->addField('general', 'position', 'Position', 'text', ['required' => false]);
        $this->addField('general', 'description', 'Description', 'textarea', ['required' => true]);
        return parent::_beforeRender();
    }

}