<?php

namespace WFN\Blog\Block\Admin\Author;

class Grid extends \WFN\Blog\Block\Admin\BlogEntity\Grid
{

    protected $filterableFields = ['status', 'name', 'position', 'url_key'];

    protected $adminRoute = 'admin.blog.author';

    protected $entityType = 'author';

    public function getInstance()
    {
        return new \BlogAuthor();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('status', 'Status', 'select', true, new \WFN\CMS\Model\Source\Status());
        $this->addColumn('name', 'Name');
        $this->addColumn('position', 'Position');
        $this->addColumn('url_key', 'URL Key');
        return parent::_beforeRender();
    }

    public function getTitle()
    {
        return 'Manage ' . (\Settings::getConfigValue('blog/authors_navigation_label') ?: 'Authors');
    }

}