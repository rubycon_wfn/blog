<?php

Route::prefix('api/v1')->middleware('api')->group(function() {
    Route::prefix('blog')->group(function() {
        Route::prefix('post')->group(function() {
            Route::get('list', WFN\Blog\Http\Controllers\Api\PostController::class . '@list');
            Route::get('detail/{post}', WFN\Blog\Http\Controllers\Api\PostController::class . '@detail');
        });

        Route::prefix('category')->group(function() {
            Route::get('list', WFN\Blog\Http\Controllers\Api\CategoryController::class . '@list');
            Route::get('detail/{category}', WFN\Blog\Http\Controllers\Api\CategoryController::class . '@detail');
            Route::get('{category}/posts', WFN\Blog\Http\Controllers\Api\CategoryController::class . '@posts');
        });

        Route::prefix('author')->group(function() {
            Route::get('list', WFN\Blog\Http\Controllers\Api\AuthorController::class . '@list');
            Route::get('detail/{author}', WFN\Blog\Http\Controllers\Api\AuthorController::class . '@detail');
            Route::get('{author}/posts', WFN\Blog\Http\Controllers\Api\AuthorController::class . '@posts');
        });

        Route::prefix('tag')->group(function() {
            Route::get('list', WFN\Blog\Http\Controllers\Api\TagController::class . '@list');
            Route::get('detail/{tag}', WFN\Blog\Http\Controllers\Api\TagController::class . '@detail');
            Route::get('{tag}/posts', WFN\Blog\Http\Controllers\Api\TagController::class . '@posts');
        });
    });
});
