<?php

$blogRoutes = [
    'post'     => '\WFN\Blog\Http\Controllers\Admin\PostController',
    'category' => '\WFN\Blog\Http\Controllers\Admin\CategoryController',
    'tag'      => '\WFN\Blog\Http\Controllers\Admin\TagController',
    'author'   => '\WFN\Blog\Http\Controllers\Admin\AuthorController',
];

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() use ($blogRoutes) {
    Route::prefix('blog')->group(function() use ($blogRoutes) {
        foreach($blogRoutes as $route => $controller) {
            Route::prefix($route)->group(function() use ($route, $controller) {
                Route::get('/', $controller . '@index')->name('admin.blog.' . $route);
                Route::get('/edit/{id}', $controller . '@edit')->name('admin.blog.' . $route . '.edit');
                Route::get('/new', $controller . '@new')->name('admin.blog.' . $route . '.new');
                Route::post('/save', $controller . '@save')->name('admin.blog.' . $route . '.save');
                Route::get('/delete/{id}', $controller . '@delete')->name('admin.blog.' . $route . '.delete');
            });
        }
    });
});

$blogRoute = \Settings::getConfigValue('blog/route');
Route::prefix($blogRoute ?: 'blog')->group(function() {
    Route::get('/', '\WFN\Blog\Http\Controllers\IndexController@index')->name('blog.index');

    Route::get('/post/view/{post}/{category?}', '\WFN\Blog\Http\Controllers\PostController@view')->name('blog.post');
    Route::get('/post/view/{post}/category/{category}', '\WFN\Blog\Http\Controllers\PostController@viewWithCategory')->name('blog.post.category');
    Route::get('/post/view/{post}/author/{author}', '\WFN\Blog\Http\Controllers\PostController@viewWithAuthor')->name('blog.post.author');
    Route::get('/post/view/{post}/tag/{tag}', '\WFN\Blog\Http\Controllers\PostController@viewWithTag')->name('blog.post.tag');

    Route::get('/category/view/{category}', '\WFN\Blog\Http\Controllers\CategoryController@view')->name('blog.category');
    Route::get('/author/view/{author}', '\WFN\Blog\Http\Controllers\AuthorController@view')->name('blog.author');
    Route::get('/tag/view/{tag}', '\WFN\Blog\Http\Controllers\TagController@view')->name('blog.tag');

    Route::any('{any}', function($request) {
        return App::make(\WFN\Blog\Http\Router::class)->handle($request);
    })->where('any', '(.)*');
});