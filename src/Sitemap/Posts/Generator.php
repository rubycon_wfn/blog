<?php

namespace WFN\Blog\Sitemap\Posts;

use WFN\CMS\Model\Source\Status;

class Generator extends \WFN\Sitemap\Model\Generator
{
    
    const TITLE = 'Blog Posts';

    protected $links = [];

    protected $lastMod;

    public function getLastMod()
    {
        $this->_init();
        return $this->lastMod;
    }

    public function getLinks()
    {
        $this->_init();
        return $this->links;
    }

    protected function _init()
    {
        if(!$this->links) {
            foreach($this->_getCollection()->where('status', Status::ENABLED)->get() as $entity) {
                if(!$this->lastMod || $this->lastMod->lt($entity->updated_at)) {
                    $this->lastMod = $entity->updated_at;
                }
                $this->links[] = (object)[
                    'loc'     => $this->_getUrlLink($entity),
                    'lastmod' => $entity->updated_at
                ];
            }
        }
    }

    protected function _getCollection()
    {
        return \BlogPost::query();
    }

    protected function _getUrlLink($entity)
    {
        return url($entity->getUrlLink());
    }

}