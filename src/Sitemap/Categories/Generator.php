<?php

namespace WFN\Blog\Sitemap\Categories;

class Generator extends \WFN\Blog\Sitemap\Posts\Generator
{
    
    const TITLE = 'Blog Categories';

    protected function _getCollection()
    {
        return \BlogCategory::query();
    }

    protected function _getUrlLink($entity)
    {
        return url($entity->getUrlPath());
    }

}