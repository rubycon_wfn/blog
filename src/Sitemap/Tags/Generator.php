<?php

namespace WFN\Blog\Sitemap\Tags;

class Generator extends \WFN\Blog\Sitemap\Authors\Generator
{
    
    const TITLE = 'Blog Tags';

    protected function _getCollection()
    {
        return \BlogTag::query();
    }

}