<?php

namespace WFN\Blog\Sitemap\Authors;

class Generator extends \WFN\Blog\Sitemap\Posts\Generator
{
    
    const TITLE = 'Blog Authors';

    protected function _getCollection()
    {
        return \BlogAuthor::query();
    }

    protected function _getUrlLink($entity)
    {
        return $entity->getUrlLink();
    }

}