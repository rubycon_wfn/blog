<?php

namespace WFN\Blog\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BlogEntityValidationRules
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $rules;

    public $entity;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $rules, $entity)
    {
        $this->rules = $rules;
        $this->entity = $entity;
    }

}
