<?php

namespace WFN\Blog\Model\Category;

class Detail extends \WFN\Blog\Model\BlogEntity\Detail
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;

    protected $entityType = 'category';

    protected $table = 'blog_category_detail';

}