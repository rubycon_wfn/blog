<?php

namespace WFN\Blog\Model;

/**
 * @SWG\Definition(
 *  definition="Category",
 *  description="Blog Category",
 *  @SWG\Property(property="id", type="integer"),
 *  @SWG\Property(property="parent_id", type="integer"),
 *  @SWG\Property(property="image", type="string"),
 *  @SWG\Property(property="title", type="string"),
 *  @SWG\Property(property="content", type="string"),
 *  @SWG\Property(property="status", type="integer"),
 *  @SWG\Property(property="created_at", type="string"),
 *  @SWG\Property(property="updated_at", type="string")
 * )
 */
class Category extends BlogEntity
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;

    protected $table = 'blog_category';

    protected $fillable = ['parent_id', 'status', 'image', 'title', 'content'];

    protected $mediaFields = ['image'];

    public function parent()
    {
        return $this->hasOne(\BlogCategory::class, 'id', 'parent_id');
    }

    public function childrens()
    {
        return $this->hasMany(\BlogCategory::class, 'parent_id', 'id');
    }

    public function posts()
    {
        return $this->hasManyThrough(
            \BlogPost::class,
            \WFN\Blog\Model\Post\Category::class,
            'category_id', 'id', 'id', 'post_id'
        );
    }

    public function getUrlPath()
    {
        if(!$this->path) {
            $path = [$this->url_key];
            $currentCategory = $this;
            while($currentCategory->parent) {
                $path[] = $currentCategory->parent->url_key;
                $currentCategory = $currentCategory->parent;
            }
            $path[] = \Settings::getConfigValue('blog/route') ?: 'blog';
            $path = array_reverse($path);
            $this->path = '/' . implode('/', $path);
        }
        return $this->path;
    }

    public function toArray()
    {
        $data = parent::toArray();
        $data['url_link'] = url($this->getUrlPath());
        return $data;
    }

}