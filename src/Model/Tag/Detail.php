<?php

namespace WFN\Blog\Model\Tag;

class Detail extends \WFN\Blog\Model\BlogEntity\Detail
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'tag' . DIRECTORY_SEPARATOR;

    protected $entityType = 'tag';

    protected $table = 'blog_tag_detail';

}