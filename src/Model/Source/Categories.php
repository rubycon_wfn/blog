<?php
namespace WFN\Blog\Model\Source;

use WFN\CMS\Model\Source\Status;

class Categories extends \WFN\Admin\Model\Source\AbstractSource
{

    protected $_options = [];

    protected $markDisabled = true;

    protected $titleAttribute = 'title';

    protected function _getOptions()
    {
        if(!$this->_options) {
            foreach($this->getItems() as $item) {
                $this->_options[$item->id] = $item->getAttribute($this->titleAttribute) . ($this->markDisabled && $item->status == Status::DISABLED ? ' (Disabled)' : '');
                $this->_addChildrens($item, '-');
            }
        }
        return $this->_options;
    }

    protected function _addChildrens($parent, $prefix)
    {
        if(!$parent->childrens) {
            return ;
        }

        foreach($parent->childrens as $item) {
            $this->_options[$item->id] = $prefix . ' ' . $item->getAttribute($this->titleAttribute) . ($this->markDisabled && $item->status == Status::DISABLED ? ' (Disabled)' : '');
            $this->_addChildrens($item, $prefix . '-');
        }
    }

    protected function getItems()
    {
        return \BlogCategory::whereNull('parent_id')->get();
    }

}