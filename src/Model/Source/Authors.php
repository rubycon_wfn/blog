<?php
namespace WFN\Blog\Model\Source;

class Authors extends Categories
{

    protected $markDisabled = false;

    protected $titleAttribute = 'name';

    protected function getItems()
    {
        return \BlogAuthor::all();
    }

}