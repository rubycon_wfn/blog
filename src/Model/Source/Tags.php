<?php
namespace WFN\Blog\Model\Source;

class Tags extends Categories
{

    protected function getItems()
    {
        return \BlogTag::all();
    }

}