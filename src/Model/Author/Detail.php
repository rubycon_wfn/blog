<?php

namespace WFN\Blog\Model\Author;

class Detail extends \WFN\Blog\Model\BlogEntity\Detail
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'author' . DIRECTORY_SEPARATOR;

    protected $entityType = 'author';

    protected $table = 'blog_author_detail';

}