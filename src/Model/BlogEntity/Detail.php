<?php

namespace WFN\Blog\Model\BlogEntity;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Detail extends Model
{

    const MEDIA_PATH = '';

    protected $entityType;

    protected $fillable = [];

    protected $mediaFields = [];

    protected $dates = ['created_at', 'updated_at'];

    protected $serviceColumns = ['id', 'created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        if(!$this->entityType) {
            throw new \Exception('Entity type is required');
        }

        if(!static::MEDIA_PATH) {
            throw new \Exception('Media Path is required');
        }
        
        $this->serviceColumns[] = $this->entityType . '_id'; 
        $this->hidden = $this->serviceColumns;
        
        $columns = \DB::select('DESCRIBE ' . $this->table);
        foreach($columns as $column) {
            if(!in_array($column->Field, $this->serviceColumns)) {
                $this->fillable[] = $column->Field;
            }
        }

        foreach (config($this->entityType . 'DetailFields', []) as $group => $fields) {
            foreach ($fields as $field => $data) {
                if($data['type'] == 'date') {
                    $this->dates[] = $field;
                }
                if($data['type'] == 'image' || $data['type'] == 'file') {
                    $this->mediaFields[] = $field;
                }
            }
        }

        return parent::__construct($attributes);
    }

    public function isFillable($field)
    {
        return in_array($field, $this->fillable);
    }

    public function fill(array $attributes)
    {
        $_attributes = [];
        foreach($attributes as $key => $value) {
            if(!$this->isFillable($key)) {
                continue;
            }
            $_attributes[$key] = $value;
            if(in_array($key, $this->mediaFields) && !empty($value['file']) && $value['file'] instanceof \Illuminate\Http\UploadedFile) {
                $value = $this->_uploadFile($value['file']);
                $_attributes[$key] = $value;
            }
        }
        return parent::fill($_attributes);
    }

    public function getAttributeUrl($key)
    {
        $value = $this->getAttribute($key);
        return $value ? Storage::url(static::MEDIA_PATH . $value) : false;
    }

    protected function _uploadFile($file)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . static::MEDIA_PATH;
        $fileName = $file->getClientOriginalName();
        $path .= substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR;
        if(Storage::disk('local')->exists($path . $fileName)) {
            $iterator = 1;
            while(Storage::disk('local')->exists($path . $fileName)) {
                $fileName = str_replace(
                    '.' . $file->getClientOriginalExtension(),
                    $iterator++ . '.' . $file->getClientOriginalExtension(),
                    $file->getClientOriginalName()
                );
            }
        }

        $file->storeAs($path, $fileName);
        return substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR . $fileName;
    }

}