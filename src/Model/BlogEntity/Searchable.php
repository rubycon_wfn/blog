<?php

namespace WFN\Blog\Model\BlogEntity;

trait Searchable
{

    public static function bootSearchable()
    {
        if (config('services.search.enabled')) {
            static::observe(\WFN\Blog\Observer\Elasticsearch::class);
        }
    }

    public function getSearchIndex()
    {
        return config('services.search.index_prefix') . $this->getTable();
    }

    public function getSearchType()
    {
        if (property_exists($this, 'useSearchType')) {
            return $this->useSearchType;
        }

        return $this->getTable();
    }

    public function toSearchArray()
    {
        return $this->toArray();
    }

}