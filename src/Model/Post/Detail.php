<?php

namespace WFN\Blog\Model\Post;

class Detail extends \WFN\Blog\Model\BlogEntity\Detail
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'post' . DIRECTORY_SEPARATOR;

    protected $entityType = 'post';

    protected $table = 'blog_post_detail';

}