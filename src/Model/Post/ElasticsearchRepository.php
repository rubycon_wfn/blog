<?php

namespace WFN\Blog\Model\Post;

use BlogPost;
use Elasticsearch\Client;
use WFN\CMS\Model\Source\Status;

class ElasticsearchRepository implements Repository
{
    private $search;

    public function __construct(Client $client) {
        $this->search = $client;
    }

    public function search(string $query = "", $page = 1)
    {
        $items = $this->searchOnElasticsearch($query, $page);

        return $this->buildCollection($items);
    }

    private function searchOnElasticsearch(string $query, $page)
    {
        $instance = new BlogPost;

        $items = $this->search->search([
            'index' => $instance->getSearchIndex(),
            'type' => $instance->getSearchType(),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'multi_match' => [
                                'fields' => ['title^5', 'content', 'author_name', 'category_titles', 'tag_titles'],
                                'query' => $query,
                            ],
                        ],
                        'filter' => [
                            ['term' => ['status' => Status::ENABLED]]
                        ],
                    ],
                ],
                'from' => max(min(intval($page), 500) - 1, 0) * 12,
                'size' => 12,
            ],
        ]);

        return $items;
    }

    private function buildCollection(array $items)
    {
        $hits = array_pluck($items['hits']['hits'], '_source') ?: [];

        $sources = array_map(function ($source) {
            $source['author'] = (new \BlogAuthor())->fill($source['author']);
            $source['categories'] = \BlogCategory::hydrate($source['categories']);
            $source['tags'] = \BlogTag::hydrate($source['tags']);
            if(!empty($source['detail'])) {
                $source['detail'] = (new Detail())->fill($source['detail']);
            }
            return $source;
        }, $hits);

        $posts = BlogPost::hydrate($sources);
        $posts->total = $items['hits']['total'];

        return $posts;
    }
}