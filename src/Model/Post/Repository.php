<?php
namespace WFN\Blog\Model\Post;

interface Repository
{
    public function search(string $query = "");
}