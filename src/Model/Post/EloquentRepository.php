<?php

namespace WFN\Blog\Model\Post;

use BlogPost;
use WFN\CMS\Model\Source\Status;

class EloquentRepository implements Repository
{
    public function search(string $query = "")
    {
        return BlogPost::where('content', 'like', "%{$query}%")
            ->where('status', Status::ENABLED)
            ->orWhere('title', 'like', "%{$query}%")
            ->paginate(12);
    }
}