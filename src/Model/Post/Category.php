<?php

namespace WFN\Blog\Model\Post;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['post_id', 'category_id'];

    protected $table = 'blog_post_category';

}