<?php

namespace WFN\Blog\Model\Post;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $fillable = ['post_id', 'tag_id'];

    protected $table = 'blog_post_tag';

}