<?php

namespace WFN\Blog\Model;

use WFN\Blog\Model\BlogEntity\Searchable;

/**
 * @SWG\Definition(
 *  definition="Post",
 *  description="Blog Post",
 *  @SWG\Property(property="id", type="integer"),
 *  @SWG\Property(property="thumbnail", type="string"),
 *  @SWG\Property(property="title", type="string"),
 *  @SWG\Property(property="content", type="string"),
 *  @SWG\Property(property="url_key", type="string"),
 *  @SWG\Property(property="status", type="integer"),
 *  @SWG\Property(property="created_at", type="string"),
 *  @SWG\Property(property="updated_at", type="string"),
 *  @SWG\Property(property="author", ref="#/definitions/Author"),
 *  @SWG\Property(property="categories", type="array", @SWG\Items(ref="#/definitions/Category")),
 *  @SWG\Property(property="tags", type="array", @SWG\Items(ref="#/definitions/Tag")),
 * )
 */
class Post extends BlogEntity
{

    use Searchable;

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'post' . DIRECTORY_SEPARATOR;

    protected $table = 'blog_post';

    protected $fillable = ['author_id', 'status', 'thumbnail', 'title', 'content'];

    protected $mediaFields = ['thumbnail'];

    protected $_tagIds;

    protected $_categoryIds;

    protected $_category;

    protected $_author;

    protected $_tag;

    public function getIdAttribute()
    {
        return $this->getAttributeFromArray('post_id') ?: $this->getAttributeFromArray('id');
    }

    public function getAttribute($key)
    {
        switch($key) {
            case 'category_ids':
                return $this->getRelationIds('categories');
            case 'tag_ids':
                return $this->getRelationIds('tags');
            default:
                return parent::getAttribute($key);
        }
    }

    public function fill(array $attributes)
    {
        if(!empty($attributes['category_ids'])) {
            $this->_categoryIds = $attributes['category_ids'];
        }
        if(!empty($attributes['tag_ids'])) {
            $this->_tagIds = $attributes['tag_ids'];
        }
        return parent::fill($attributes);
    }

    public function categories()
    {
        return $this->hasManyThrough(
            \BlogCategory::class,
            \WFN\Blog\Model\Post\Category::class,
            'post_id', 'id', 'id', 'category_id'
        );
    }

    public function tags()
    {
        return $this->hasManyThrough(
            \BlogTag::class,
            \WFN\Blog\Model\Post\Tag::class,
            'post_id', 'id', 'id', 'tag_id'
        );
    }

    public function author()
    {
        return $this->hasOne(\BlogAuthor::class, 'id', 'author_id');
    }

    public function setCategory($category)
    {
        $this->_category = $category;
        return $this;
    }

    public function setAuthor($author)
    {
        $this->_author = $author;
        return $this;
    }

    public function setTag($tag)
    {
        $this->_tag = $tag;
        return $this;
    }

    public function getUrlLink()
    {
        $path = '';
        if(!empty($this->_category)) {
            $path .= $this->_category->getUrlPath();
        } elseif(!empty($this->_author)) {
            $path .= $this->_author->getUrlLink();
        } elseif(!empty($this->_tag)) {
            $path .= $this->_tag->getUrlLink();
        } else {
            $path .= '/' . \Settings::getConfigValue('blog/route') ?: 'blog';
        }
        $path .= '/';
        return $path . $this->url_key;
    }

    public function getRelationIds($relation)
    {
        $values = [];
        if($this->{$relation}) {
            foreach($this->{$relation} as $_relation) {
                $values[] = $_relation->id;
            }
        }
        return $values;
    }

    protected function _afterSave()
    {
        $this->_updateTaxonomyRelations('category', $this->_categoryIds);
        $this->_updateTaxonomyRelations('tag', $this->_tagIds);
         return parent::_afterSave();
    }

    protected function _updateTaxonomyRelations($relation, $ids)
    {
        if(empty($ids)) {
            return $this;
        }
        $relationClass = '\WFN\Blog\Model\Post\\' . ucfirst($relation);
        $_relation = new $relationClass();
        
        // Remove old relations
        $_relation->where('post_id', $this->id)->delete();
        
        // Add new relations
        foreach($ids as $id) {
            $_relation = new $relationClass();
            $_relation->fill([
                'post_id'         => $this->id,
                $relation . '_id' => $id,
            ])->save();
        }
    }

    public function toArray()
    {
        $data = parent::toArray();
        $data['author'] = $this->author;
        $data['author_name'] = $this->author->name;
        $data['categories'] = $this->categories;
        $data['category_titles'] = [];
        foreach($this->categories as $category) {
            $data['category_titles'][] = $category->title;
        }
        $data['category_titles'] = implode(',', $data['category_titles']);
        $data['tags'] = $this->tags;
        $data['tag_titles'] = [];
        foreach($this->tags as $tag) {
            $data['tag_titles'][] = $tag->title;
        }
        $data['tag_titles'] = implode(',', $data['tag_titles']);
        return $data;
    }

}