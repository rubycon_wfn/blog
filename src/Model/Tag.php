<?php

namespace WFN\Blog\Model;

/**
 * @SWG\Definition(
 *  definition="Tag",
 *  description="Blog Tag",
 *  @SWG\Property(property="id", type="integer"),
 *  @SWG\Property(property="title", type="string"),
 *  @SWG\Property(property="content", type="string"),
 *  @SWG\Property(property="status", type="integer"),
 *  @SWG\Property(property="created_at", type="string"),
 *  @SWG\Property(property="updated_at", type="string")
 * )
 */
class Tag extends BlogEntity
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'tag' . DIRECTORY_SEPARATOR;

    protected $table = 'blog_tag';

    protected $fillable = ['status', 'title', 'content'];

    public function posts()
    {
        return $this->hasManyThrough(
            \BlogPost::class,
            \WFN\Blog\Model\Post\Tag::class,
            'tag_id', 'id', 'id', 'post_id'
        );
    }

    public function getUrlLink()
    {
        return url((\Settings::getConfigValue('blog/route') ?: 'blog') . '/' . $this->url_key);
    }

}