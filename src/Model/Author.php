<?php

namespace WFN\Blog\Model;

/**
 * @SWG\Definition(
 *  definition="Author",
 *  description="Blog Author",
 *  @SWG\Property(property="id", type="integer"),
 *  @SWG\Property(property="status", type="integer"),
 *  @SWG\Property(property="photo", type="string"),
 *  @SWG\Property(property="name", type="string"),
 *  @SWG\Property(property="position", type="string"),
 *  @SWG\Property(property="description", type="string"),
 *  @SWG\Property(property="created_at", type="string"),
 *  @SWG\Property(property="updated_at", type="string")
 * )
 */
class Author extends BlogEntity
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR . 'author' . DIRECTORY_SEPARATOR;

    protected $table = 'blog_author';

    protected $fillable = ['status', 'photo', 'name', 'position', 'description'];

    protected $mediaFields = ['photo'];

    public function posts()
    {
        return $this->hasMany(
            \BlogPost::class,
            'author_id', 'id'
        );
    }

    public function getUrlLink()
    {
        return url((\Settings::getConfigValue('blog/route') ?: 'blog') . '/' . $this->url_key);
    }

}