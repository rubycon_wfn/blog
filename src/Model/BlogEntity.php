<?php

namespace WFN\Blog\Model;

use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\Model;
use Storage;

class BlogEntity extends Model
{

    const MEDIA_PATH = 'blog' . DIRECTORY_SEPARATOR;

    protected $mediaFields = [];

    protected $allData = [];

    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, [
            'url_key', 'meta_title', 'meta_description', 'og_title', 'og_description', 'og_image',
        ]);
        $this->mediaFields[] = 'og_image';
    }

    public function fill(array $attributes)
    {
        $this->allData = $attributes;
        foreach($attributes as $key => $value) {
            if(in_array($key, $this->mediaFields) && !empty($value['file']) && $value['file'] instanceof \Illuminate\Http\UploadedFile) {
                $value = $this->_uploadFile($value['file']);
                $attributes[$key] = $value;
            }
        }
        return parent::fill($attributes);
    }

    public function detail()
    {
        $path = explode('\\', static::class);
        return $this->hasOne('\WFN\Blog\Model\\' . array_pop($path) . '\Detail');
    }

    public function getContent()
    {
        if(!$this->_filteredContent) {
            $filter = new \WFN\CMS\Model\Content\Filter();
            $this->_filteredContent = $filter->filterContent($this->content);
        }
        return $this->_filteredContent;
    }

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if(is_null($value) && !empty($this->detail)) {
            $value = $this->detail->getAttribute($key);
        }
        return $value;
    }

    public function getAttributeUrl($key)
    {
        $value = $this->getAttribute($key);
        return $value ? Request::root() . Storage::url(str_replace(DIRECTORY_SEPARATOR, '/', static::MEDIA_PATH) . $value) : false;
    }

    public function save(array $options = [])
    {
        $this->_beforeSave();
        parent::save($options);
        $this->_afterSave();
        return $this;
    }

    protected function _uploadFile($file)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . static::MEDIA_PATH;
        $fileName = $file->getClientOriginalName();
        $path .= substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR;
        if(Storage::disk('local')->exists($path . $fileName)) {
            $iterator = 1;
            while(Storage::disk('local')->exists($path . $fileName)) {
                $fileName = str_replace(
                    '.' . $file->getClientOriginalExtension(),
                    $iterator++ . '.' . $file->getClientOriginalExtension(),
                    $file->getClientOriginalName()
                );
            }
        }

        $file->storeAs($path, $fileName);
        return substr($fileName, 0, 1) . '/' . substr($fileName, 1, 1) . '/' . $fileName;
    }

    protected function _beforeSave()
    {
        return $this;
    }

    protected function _afterSave()
    {
        if(empty($this->detail)) {
            $this->detail()->create();
            $this->load('detail');
        }
        $data = $this->allData;
        $data['id'] = null;
        $this->detail->fill($data)->save();
        return $this;
    }

    public function toArray()
    {
        $data = parent::toArray();
        if($this->detail) {
            $data = array_merge($this->detail->toArray(), $data);
        }
        return $data;
    }

    public function getMetaTitle()
    {
        return $this->meta_title ?: $this->title ?: $this->name;
    }

    public function getMetaDescription()
    {
        return $this->meta_description ?: substr(strip_tags($this->getContent()), 0, 250) ?: substr($this->description, 0, 250);
    }

    public function getOgTitle()
    {
        return $this->og_title ?: $this->getMetaTitle();
    }

    public function getOgDescription()
    {
        return $this->og_description ?: $this->getMetaDescription();
    }

    public function getOgImageUrl()
    {
        return $this->getAttributeUrl('og_image');
    }

}