<?php

namespace WFN\Blog\Helper;

class Category
{

    protected static $categoriesTree;

    protected static $categoryNodePositions = [];

    protected static $nodePosition = 0;

    public static function getCategoriesTreeData()
    {
        static::$categoriesTree = [];
        $categories = \BlogCategory::whereNull('parent_id')->get();
        foreach($categories as $category) {
            $node = [
                'text' => $category->title,
                'href' => route('admin.blog.category.edit', ['id' => $category->id])
            ];
            static::$categoryNodePositions[$category->id] = static::$nodePosition++;
            if($category->childrens()->count()) {
                $node = static::_buildTreeRecursively($node, $category->childrens);
            }
            static::$categoriesTree[] = $node;
        }
        return static::$categoriesTree;
    }

    public static function getSelectedNodeNumber($currenctCategoryId)
    {
        return isset(static::$categoryNodePositions[$currenctCategoryId]) ? static::$categoryNodePositions[$currenctCategoryId] : false;
    }

    protected static function _buildTreeRecursively($node, $childrens)
    {
        $node['nodes'] = [];
        foreach($childrens as $category) {
            $_node = [
                'text' => $category->title,
                'href' => route('admin.blog.category.edit', ['id' => $category->id])
            ];
            static::$categoryNodePositions[$category->id] = static::$nodePosition++;
            if($category->childrens()->count()) {
                $_node = static::_buildTreeRecursively($_node, $category->childrens);
            }
            $node['nodes'][] = $_node;
        }
        return $node;
    }

}