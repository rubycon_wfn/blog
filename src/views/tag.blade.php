@extends('layouts.app')

@section('meta_title', $tag->getMetaTitle())
@section('meta_description', $tag->getMetaDescription())
@section('og_data')
    <meta property="og:title" content="{{ $tag->getOgTitle() }}"/>
    <meta property="og:description" content="{{ $tag->getOgDescription() }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{ url($tag->getUrlLink()) }}"/>
    @if($tag->og_image)
        <meta property="og:image" content="{{ $tag->getOgImageUrl() }}"/>
    @endif
@endsection

@section('content')
<div class="container">
    {!! $breadcrumbs->render() !!}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $tag->title }}</div>
            </div>
            <div class="card-body">
                {!! $tag->getContent() !!}
            </div>
        </div>
    </div>
</div>
@endsection