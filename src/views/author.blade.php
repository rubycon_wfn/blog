@extends('layouts.app')

@section('meta_title', $author->getMetaTitle())
@section('meta_description', $author->getMetaDescription())
@section('og_data')
    <meta property="og:title" content="{{ $author->getOgTitle() }}"/>
    <meta property="og:description" content="{{ $author->getOgDescription() }}"/>
    <meta property="og:type" content="author"/>
    <meta property="og:url" content="{{ $author->getUrlLink() }}"/>
    @if($author->og_image)
        <meta property="og:image" content="{{ $author->getOgImageUrl() }}"/>
    @endif
@endsection

@section('content')
<div class="container">
    {!! $breadcrumbs->render() !!}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $author->name }}</div>
            </div>
            <div class="card-body">
                {{ $author->description }}
            </div>
        </div>
    </div>
</div>
@endsection