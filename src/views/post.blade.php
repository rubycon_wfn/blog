@extends('layouts.app')

@section('meta_title', $post->getMetaTitle())
@section('meta_description', $post->getMetaDescription())
@section('og_data')
    <meta property="og:title" content="{{ $post->getOgTitle() }}"/>
    <meta property="og:description" content="{{ $post->getOgDescription() }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{ url($post->getUrlLink()) }}"/>
    @if($post->og_image)
        <meta property="og:image" content="{{ $post->getOgImageUrl() }}"/>
    @endif
@endsection

@section('content')
<div class="container">
    {!! $breadcrumbs->render() !!}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $post->title }}</div>
            </div>
            <div class="card-body">
                {!! $post->getContent() !!}
            </div>
        </div>
    </div>
</div>
@endsection