<div id="category_tree" class=""></div>
<script type="text/javascript">
$(function() {
    var defaultData = {!! json_encode(\WFN\Blog\Helper\Category::getCategoriesTreeData()) !!}
    var tree = $('#category_tree');

    tree.treeview({
        expandIcon: "fa fa-plus",
        collapseIcon: "fa fa-minus",
        enableLinks: @if(Auth::guard('admin')->user()->role->isAvailable('admin.blog.category.edit')) true @else false @endif,
        levels: 1,
        data: defaultData
    });

    @if(($selectedNode = \WFN\Blog\Helper\Category::getSelectedNodeNumber($form->getInstance()->id)) !== false)
    tree.treeview('selectNode', [{{ $selectedNode }}]);
    tree.treeview('revealNode', [{{ $selectedNode }}]);
    @endif
});
</script>