@extends('admin::layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9"><h4 class="page-title">{{ $form->getTitle() }}</h4></div>
        <div class="col-sm-3">
            <div class="btn-group float-sm-right">
            @foreach ($form->getButtons() as $data)
                @includeIf('admin::widget.button', $data)
            @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    @include('blog::admin.category.tree')
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card">
                @if($edit)
                    @include('admin::widget.form.content')
                @elseif(Auth::guard('admin')->user()->role->isAvailable('admin.blog.category.edit'))
                    <div class="card-body">
                        {{ __('Please, select category to edit') }}
                    </div>
                @else
                    <div class="card-body">
                        {{ __('You have no access to this section') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection