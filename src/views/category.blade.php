@extends('layouts.app')

@section('meta_title', $category->getMetaTitle())
@section('meta_description', $category->getMetaDescription())
@section('og_data')
    <meta property="og:title" content="{{ $category->getOgTitle() }}"/>
    <meta property="og:description" content="{{ $category->getOgDescription() }}"/>
    <meta property="og:type" content="category"/>
    <meta property="og:url" content="{{ url($category->getUrlPath()) }}"/>
    @if($category->og_image)
        <meta property="og:image" content="{{ $category->getOgImageUrl() }}"/>
    @endif
@endsection

@section('content')
<div class="container">
    {!! $breadcrumbs->render() !!}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $category->title }}</div>
            </div>
            <div class="card-body">
                {!! $category->getContent() !!}
            </div>
        </div>
    </div>
</div>
@endsection