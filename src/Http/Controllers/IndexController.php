<?php

namespace WFN\Blog\Http\Controllers;

use Illuminate\Support\Facades\View;

class IndexController extends BaseController
{

    public function index()
    {
        $breadcrumbs = \WFN\Blog\Block\Breadcrumbs::getInstance();
        return view(View::exists('blog.index') ? 'blog.index' : 'blog::index', compact('breadcrumbs'));
    }

}