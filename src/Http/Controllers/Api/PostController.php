<?php

namespace WFN\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;

class PostController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Get(
     *     path="/blog/post/list",
     *     summary="Get blog posts",
     *     tags={"Blog Post"},
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Post")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     * )
     */
    public function list(Request $request)
    {
        return \BlogPost::paginate($request->input('limit') ?: 10);
    }

    /**
     * @SWG\Get(
     *     path="/blog/post/detail/{post}",
     *     summary="Get post detail information",
     *     tags={"Blog Post"},
     *     @SWG\Parameter(name="post", description="Post Id", required=true, type="integer", in="path"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Post"),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function detail($postId)
    {
        $post = \BlogPost::find($postId);
        return !empty($post) ? $post : ['error' => 'No such entity found'];
    }
}