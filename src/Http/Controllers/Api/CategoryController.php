<?php

namespace WFN\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;

class CategoryController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Get(
     *     path="/blog/category/list",
     *     summary="Get blog categories",
     *     tags={"Blog Category"},
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Category")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     * )
     */
    public function list(Request $request)
    {
        return \BlogCategory::paginate($request->input('limit') ?: 10);
    }

    /**
     * @SWG\Get(
     *     path="/blog/category/detail/{category}",
     *     summary="Get category detail information",
     *     tags={"Blog Category"},
     *     @SWG\Parameter(name="category", description="Category Id", required=true, type="integer", in="path"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Category"),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function detail($categoryId)
    {
        $category = \BlogCategory::find($categoryId);
        return !empty($category) ? $category : ['error' => 'No such entity found'];
    }

    /**
     * @SWG\Get(
     *     path="/blog/category/{category}/posts",
     *     summary="Get category posts",
     *     tags={"Blog Category"},
     *     @SWG\Parameter(name="category", description="Category Id", required=true, type="integer", in="path"),
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Post")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function posts(Request $request, $categoryId)
    {
        $category = \BlogCategory::find($categoryId);
        return !empty($category) ? $category->posts()->paginate($request->input('limit') ?: 10) : ['error' => 'No such entity found'];
    }

}