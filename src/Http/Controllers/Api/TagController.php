<?php

namespace WFN\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;

class TagController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Get(
     *     path="/blog/tag/list",
     *     summary="Get blog tags",
     *     tags={"Blog Tag"},
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Tag")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     * )
     */
    public function list(Request $request)
    {
        return \BlogTag::paginate($request->input('limit') ?: 10);
    }

    /**
     * @SWG\Get(
     *     path="/blog/tag/detail/{tag}",
     *     summary="Get tag detail information",
     *     tags={"Blog Tag"},
     *     @SWG\Parameter(name="tag", description="Tag Id", required=true, type="integer", in="path"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Tag"),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function detail($tagId)
    {
        $tag = \BlogTag::find($tagId);
        return !empty($tag) ? $tag : ['error' => 'No such entity found'];
    }

    /**
     * @SWG\Get(
     *     path="/blog/tag/{tag}/posts",
     *     summary="Get tag posts",
     *     tags={"Blog Tag"},
     *     @SWG\Parameter(name="tag", description="Tag Id", required=true, type="integer", in="path"),
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Post")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function posts(Request $request, $tagId)
    {
        $tag = \BlogTag::find($tagId);
        return !empty($tag) ? $tag->posts()->paginate($request->input('limit') ?: 10) : ['error' => 'No such entity found'];
    }

}