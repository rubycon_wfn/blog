<?php

namespace WFN\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;

class AuthorController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Get(
     *     path="/blog/author/list",
     *     summary="Get blog authors",
     *     tags={"Blog Author"},
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Author")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     * )
     */
    public function list(Request $request)
    {
        return \BlogAuthor::paginate($request->input('limit') ?: 10);
    }

    /**
     * @SWG\Get(
     *     path="/blog/author/detail/{author}",
     *     summary="Get author detail information",
     *     tags={"Blog Author"},
     *     @SWG\Parameter(name="author", description="Author Id", required=true, type="integer", in="path"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Author"),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function detail($authorId)
    {
        $author = \BlogAuthor::find($authorId);
        return !empty($author) ? $author : ['error' => 'No such entity found'];
    }

    /**
     * @SWG\Get(
     *     path="/blog/author/{author}/posts",
     *     summary="Get author posts",
     *     tags={"Blog Author"},
     *     @SWG\Parameter(name="author", description="Author Id", required=true, type="integer", in="path"),
     *     @SWG\Parameter(name="limit", description="Limit", required=false, type="integer", in="query"),
     *     @SWG\Parameter(name="page", description="Page", required=false, type="integer", in="query"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="current_page", type="integer"),
     *             @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Post")),
     *             @SWG\Property(property="first_page_url", type="string"),
     *             @SWG\Property(property="from", type="integer"),
     *             @SWG\Property(property="last_page", type="integer"),
     *             @SWG\Property(property="last_page_url", type="string"),
     *             @SWG\Property(property="next_page_url", type="string"),
     *             @SWG\Property(property="path", type="string"),
     *             @SWG\Property(property="per_page", type="integer"),
     *             @SWG\Property(property="prev_page_url", type="string"),
     *             @SWG\Property(property="to", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(response="404", description="No such entity found"),
     * )
     */
    public function posts(Request $request, $authorId)
    {
        $author = \BlogTag::find($authorId);
        return !empty($author) ? $author->posts()->paginate($request->input('limit') ?: 10) : ['error' => 'No such entity found'];
    }

}