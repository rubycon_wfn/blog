<?php

namespace WFN\Blog\Http\Controllers\Admin;

class TagController extends AbstractController
{
    
    protected $_entity = 'Tag';

    protected function _getValidationRules()
    {
        return [
            'title'   => 'required|string|max:255',
            'url_key' => 'required|string|max:255',
        ];
    }

}