<?php

namespace WFN\Blog\Http\Controllers\Admin;

class PostController extends AbstractController
{
    
    protected $_entity = 'Post';

    protected function _getValidationRules()
    {
        $rules = [
            'author_id' => 'required|exists:blog_author,id',
            'title'     => 'required|string|max:255',
            'content'   => 'required|string',
            'url_key'   => 'required|string|max:255',
        ];
        $event = new \WFN\Blog\Events\BlogEntityValidationRules($rules, $this->entity);
        event($event);
        return $event->rules;
    }

}