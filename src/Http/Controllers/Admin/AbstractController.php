<?php

namespace WFN\Blog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

abstract class AbstractController extends \WFN\Admin\Http\Controllers\Crud\Controller
{

    protected $_entity;

    protected function _init(Request $request)
    {
        if(!$this->_entity) {
            throw new \Exception('Entity was not set');
        }
        $gridBlockClass = '\WFN\Blog\Block\Admin\\' . $this->_entity . '\Grid';
        $formBlockClass = '\WFN\Blog\Block\Admin\\' . $this->_entity . '\Form';
        $entityClass    = '\Blog' . $this->_entity;

        $this->gridBlock   = new $gridBlockClass($request);
        $this->formBlock   = new $formBlockClass();
        $this->entity      = new $entityClass();
        $this->entityTitle = 'Blog - ' . $this->_entity;
        $this->adminRoute  = 'admin.blog.' . strtolower($this->_entity);
        return $this;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, $this->_getValidationRules());
    }

    protected abstract function _getValidationRules();

}

