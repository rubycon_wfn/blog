<?php

namespace WFN\Blog\Http\Controllers\Admin;

class AuthorController extends AbstractController
{
    
    protected $_entity = 'Author';

    protected function _getValidationRules()
    {
        return [
            'name'        => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'url_key'     => 'required|string|max:255',
        ];
    }

}