<?php

namespace WFN\Blog\Http\Controllers;

use Illuminate\Support\Facades\View;

class CategoryController extends BaseController
{

    public function view(\BlogCategory $category)
    {
        $breadcrumbs = \WFN\Blog\Block\Breadcrumbs::getInstance()->setCategory($category);
        return view(View::exists('blog.category') ? 'blog.category' : 'blog::category', compact('category', 'breadcrumbs'));
    }

}