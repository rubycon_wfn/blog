<?php

namespace WFN\Blog\Http\Controllers;

use Illuminate\Support\Facades\View;

class AuthorController extends BaseController
{

    public function view(\BlogAuthor $author)
    {
        $breadcrumbs = \WFN\Blog\Block\Breadcrumbs::getInstance()->setAuthor($author);
        return view(View::exists('blog.author') ? 'blog.author' : 'blog::author', compact('author', 'breadcrumbs'));
    }

}