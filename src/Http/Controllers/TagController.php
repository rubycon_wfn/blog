<?php

namespace WFN\Blog\Http\Controllers;

use Illuminate\Support\Facades\View;

class TagController extends BaseController
{

    public function view(\BlogTag $tag)
    {
        $breadcrumbs = \WFN\Blog\Block\Breadcrumbs::getInstance()->setTag($tag);
        return view(View::exists('blog.tag') ? 'blog.tag' : 'blog::tag', compact('tag', 'breadcrumbs'));
    }

}