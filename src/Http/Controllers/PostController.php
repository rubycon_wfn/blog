<?php

namespace WFN\Blog\Http\Controllers;

use Illuminate\Support\Facades\View;

class PostController extends BaseController
{

    public function view(\BlogPost $post)
    {
        $breadcrumbs = $this->_getBreadcrumbs($post);
        return view($this->_getView(), compact('post', 'breadcrumbs'));
    }

    public function viewWithCategory(\BlogPost $post, \BlogCategory $category)
    {
        $breadcrumbs = $this->_getBreadcrumbs($post)->setCategory($category);
        return view($this->_getView(), compact('post', 'category', 'breadcrumbs'));
    }

    public function viewWithAuthor(\BlogPost $post, \BlogAuthor $author)
    {
        $breadcrumbs = $this->_getBreadcrumbs($post)->setAuthor($author);
        return view($this->_getView(), compact('post', 'author', 'breadcrumbs'));
    }

    public function viewWithTag(\BlogPost $post, \BlogTag $tag)
    {
        $breadcrumbs = $this->_getBreadcrumbs($post)->setTag($tag);
        return view($this->_getView(), compact('post', 'tag', 'breadcrumbs'));
    }

    protected function _getView()
    {
        return View::exists('blog.post') ? 'blog.post' : 'blog::post';
    }

    protected function _getBreadcrumbs($post)
    {
        $breadcrumbs = \WFN\Blog\Block\Breadcrumbs::getInstance();
        return $breadcrumbs->setPost($post);
    }

}