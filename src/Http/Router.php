<?php
namespace WFN\Blog\Http;

use Illuminate\Routing\Router as AppRouter;
use WFN\CMS\Model\Source\Status;
use Illuminate\Http\Request;

class Router
{

    protected $appRouter;

    public function __construct(AppRouter $router)
    {
        $this->appRouter = $router;
    }

    public function handle($requestUri)
    {
        $requestUri = trim($requestUri, '/');
        $requestUri = explode('/', $requestUri);
        
        $urlKey = array_pop($requestUri);
        $blogRoute = \Settings::getConfigValue('blog/route') ?: 'blog';

        if(count($requestUri) == 0) {
            if($post = $this->_getPost($urlKey)) {
                $request = Request::create($blogRoute . '/post/view/' . $post->id);
                return $this->appRouter->dispatch($request);
            }

            if(($category = $this->_getCategory($urlKey)) && !$category->parent) {
                $request = Request::create($blogRoute . '/category/view/' . $category->id);
                return $this->appRouter->dispatch($request);
            }

            if($author = $this->_getAuthor($urlKey)) {
                $request = Request::create($blogRoute . '/author/view/' . $author->id);
                return $this->appRouter->dispatch($request);
            }

            if($tag = $this->_getTag($urlKey)) {
                $request = Request::create($blogRoute . '/tag/view/' . $tag->id);
                return $this->appRouter->dispatch($request);
            }
        } else {
            if($post = $this->_getPost($urlKey)) {
                if($category = $this->_checkPostCategory($post, $requestUri)) {
                    $request = Request::create($blogRoute . '/post/view/' . $post->id . '/category/' . $category->id);
                    return $this->appRouter->dispatch($request);
                }

                if(count($requestUri) == 1) {
                    if($author = $this->_getPostAuthor($post, $requestUri[0])) {
                        $request = Request::create($blogRoute . '/post/view/' . $post->id . '/author/' . $author->id);
                        return $this->appRouter->dispatch($request);
                    }

                    if($tag = $this->_getPostTag($post, $requestUri[0])) {
                        $request = Request::create($blogRoute . '/post/view/' . $post->id . '/tag/' . $tag->id);
                        return $this->appRouter->dispatch($request);
                    }
                }
            }

            if(($category = $this->_getCategory($urlKey)) && $this->_checkCategoryHierarchy($category, $requestUri)) {
                $request = Request::create($blogRoute . '/category/view/' . $category->id);
                return $this->appRouter->dispatch($request);
            }
        }

        return abort(404);
    }

    protected function _getPost($urlKey)
    {
        return \BlogPost::where('url_key', $urlKey)->where('status', Status::ENABLED)->first();
    }

    protected function _getCategory($urlKey)
    {
        return \BlogCategory::where('url_key', $urlKey)->where('status', Status::ENABLED)->first();
    }

    protected function _getAuthor($urlKey)
    {
        return \BlogAuthor::where('url_key', $urlKey)->first();
    }

    protected function _getTag($urlKey)
    {
        return \BlogTag::where('url_key', $urlKey)->where('status', Status::ENABLED)->first();
    }

    protected function _getPostAuthor($post, $urlKey)
    {
        return $post->author()->where('url_key', $urlKey)->first();
    }

    protected function _getPostTag($post, $urlKey)
    {
        return $post->tags()->where('url_key', $urlKey)->first();   
    }

    protected function _checkPostCategory($post, $hierarchy)
    {
        $category = $post->categories()->where('url_key', array_pop($hierarchy))->first();
        if(!$category) {
            return false;
        }
        
        if(!$this->_checkCategoryHierarchy($category, $hierarchy)) {
            return false;
        }

        return $category;
    }

    protected function _checkCategoryHierarchy($category, $hierarchy)
    {
        $currentCategory = $category;
        while($currentCategory->parent) {
            if($currentCategory->parent->url_key != array_pop($hierarchy)) {
                return false;
            }
            $currentCategory = $currentCategory->parent;
        }
        return true;
    }

}
