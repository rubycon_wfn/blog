<?php

namespace WFN\Blog\Console\Commands;

use Illuminate\Console\Command;
use BlogPost;
use Elasticsearch\Client;

class BlogIndexer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex all blog posts';

    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Indexing all articles. Might take a while...');

        foreach(BlogPost::cursor() as $model)
        {
            $this->client->index([
                'index' => $model->getSearchIndex(),
                'type' => $model->getSearchType(),
                'id' => $model->id,
                'body' => $model->toSearchArray(),
            ]);

            $this->output->write('.');
        }

        $this->info("\nDone!");
    }
    
}
